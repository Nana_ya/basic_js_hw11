const alertTrue = "You are welcome";
const alertFalse = "Потрібно ввести однакові значення";
const inputPassword = document.querySelector('input');
const confirmPassword = document.querySelectorAll('input')[1];
const inputPasswordIcons = [document.querySelector('.fa-eye'), document.querySelector('.fa-eye-slash')];
const confirmPasswordIcons = [document.querySelectorAll('.fa-eye')[1], document.querySelectorAll('.fa-eye-slash')[1]];
const buttonSubmit = document.querySelector('button');

function displayNone(elVisible, elHidden){
    elVisible.classList.toggle('hidden');
    elHidden.classList.toggle('hidden');
}

function type(input){
    if (input.type == "password") input.type = "text";
    else input.type = "password";
}

function inputPasswordEventListener(passwordIcons, password, i){
    if (i == 0){
        passwordIcons[i].addEventListener("click", () => {
            displayNone(passwordIcons[i], passwordIcons[i + 1]);
            type(password);
        });
    }
    else if(i == 1){
        passwordIcons[i].addEventListener("click", () => {
            displayNone(passwordIcons[i], passwordIcons[i - 1]);
            type(password);
        });
    }
}

inputPasswordEventListener(inputPasswordIcons, inputPassword, 0);
inputPasswordEventListener(inputPasswordIcons, inputPassword, 1);
inputPasswordEventListener(confirmPasswordIcons, confirmPassword, 0);
inputPasswordEventListener(confirmPasswordIcons, confirmPassword, 1);

buttonSubmit.onclick = () => {
    if(inputPassword.value === confirmPassword.value) alert(alertTrue);
    else alert(alertFalse);
    return false;
}